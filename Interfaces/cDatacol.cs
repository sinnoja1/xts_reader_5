﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	public class cDatacol : _cDatacol
	{
		public string Barcode
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public int Cols
		{ get { throw new NotImplementedException(); } }

		public int Density
		{ get { throw new NotImplementedException(); } }

		public string DSName
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public int DSReplicate
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public int DSSequence
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public string Instrument_Class
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public string Instrument_Name
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public string Instrument_Settings
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public cIMSProperties Properties
		{ get { throw new NotImplementedException(); } }

		public int Rows
		{ get { throw new NotImplementedException(); } }

		public string sMeasureStamp
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public DateTime TimeOffset
		{ get { throw new NotImplementedException(); }  set { throw new NotImplementedException(); } }

		public cData Add(string strWellLabel, double dblValue, string strExcept = "")
		{
			throw new NotImplementedException();
		}

		public cData Add2RowColPos(int iRow, int iCol, double dblValue, string strExcept = "")
		{
			throw new NotImplementedException();
		}

		public int count()
		{
			throw new NotImplementedException();
		}

		public dynamic Delete(object index)
		{
			throw new NotImplementedException();
		}

		public void Fill(double dblValue, string strExcept = "")
		{
			throw new NotImplementedException();
		}

		public Array GetDataAsArray()
		{
			throw new NotImplementedException();
		}

		public Array GetDataAsTypeArray()
		{
			throw new NotImplementedException();
		}

		public cData Item(object strWellLabel)
		{
			throw new NotImplementedException();
		}

		public cData ItemByRowColPos(int iRow, int iCol)
		{
			throw new NotImplementedException();
		}

		public cData Replace(string strWellLabel, double dblValue, string strExcept = "")
		{
			throw new NotImplementedException();
		}

		public string RowCol2WellName(int iRow, int iCol)
		{
			throw new NotImplementedException();
		}

		public string RowColDensity2WellName(int iRow, int iCol, int iPlateDensity)
		{
			throw new NotImplementedException();
		}

		public void SetDensity(int PlateDensity)
		{
			throw new NotImplementedException();
		}

		public void SetDimensions(int Rows, int Cols)
		{
			throw new NotImplementedException();
		}

		public int WellName2PlaceID(string sWellName)
		{
			throw new NotImplementedException();
		}

		public void WellName2RowCol(string sWellName, ref int iRow, ref int iCol)
		{
			throw new NotImplementedException();
		}
	}
}
