﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cDatacol
	{
		string Barcode { get; set; }
		int Cols { get; }
		int Density { get; }
		string DSName { get; set; }
		int DSReplicate { get; set; }
		int DSSequence { get; set; }
		string Instrument_Class { get; set; }
		string Instrument_Name { get; set; }
		string Instrument_Settings { get; set; }
		cIMSProperties Properties { get; }
		int Rows { get; }
		string sMeasureStamp { get; set; }
		DateTime TimeOffset { get; set; }
		
		cData Add(string strWellLabel, double dblValue, string strExcept = "");
		cData Add2RowColPos(int iRow, int iCol, double dblValue, string strExcept = "");
		int count();
		dynamic Delete(object index);
		void Fill(double dblValue, string strExcept = "");
		Array GetDataAsArray();
		Array GetDataAsTypeArray();
		cData Item(object strWellLabel);
		cData ItemByRowColPos(int iRow, int iCol);
		cData Replace(string strWellLabel, double dblValue, string strExcept = "");
		string RowCol2WellName(int iRow, int iCol);
		string RowColDensity2WellName(int iRow, int iCol, int iPlateDensity);
		void SetDensity(int PlateDensity);
		void SetDimensions(int Rows, int Cols);
		int WellName2PlaceID(string sWellName);
		void WellName2RowCol(string sWellName, ref int iRow, ref int iCol);
	}
}
