﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _I_DataConverter
	{
		string Name { get; }
		string Version { get; }
		
		void ConvertDataSet(ref cXTSDataSet XTSDataset);
	}
}
