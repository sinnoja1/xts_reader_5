﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cIMSProperties
	{
		IntPtr AddNullValue { set; }
		cIMSProperty Add(ref string PropName, ref string PropValue);
		int count();
		dynamic Delete(object index);
		cIMSProperty Item(object index);
		bool ItemExist(object index);
		void set_AddNullValue(ref bool value);
	}
}
