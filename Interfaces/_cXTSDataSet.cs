﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cXTSDataSet
	{
		int Cols { get; set; }
		cDatacols Datacols { get; }
		int Density { get; }
		string DriverVersionMajorMinRequired { get; }
		bool HasDatacolBarcode { get; set; }
		cIMSProperties Params { get; }
		string RawDataOrigin { get; set; }
		int Rows { get; set; }
		cIMSProperties SharedReadoutProperties { get; }
		
		void ApplyParameters();
		void ApplyPostExtractionRules();
	}
}
