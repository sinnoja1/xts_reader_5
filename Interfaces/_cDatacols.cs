﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cDatacols
	{
		void Add(ref cDatacol NewDatacol, object Key);
		int count();
		cDatacol CreateNewDataCol(object Key);
		dynamic Delete(object index);
		void FillAllDatacols(int iPlateDensity, double dblValue, string strExcept = "");
		void FillAllDatacolsWithDefaultDimensions(double dblValue, string strExcept = "");
		cDatacol Item(object index);
		void SetAllDatacolsDimensions(int Rows, int Cols);
	}
}
