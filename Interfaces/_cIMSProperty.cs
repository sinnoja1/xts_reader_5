﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cIMSProperty
	{
		string Name { get; set; }
		string Value { get; set; }
	}
}
