﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XTSReader5
{
	interface _cData
	{
		string WExcept { get; set; }
		string Wlabel { get; set; }
		double WValue { get; set; }
	}
}
